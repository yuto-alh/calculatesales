package jp.alhinc.katagiri_yuto.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;


public class CalculateSales {


	public static void main(String[] args) throws IOException {
		ArrayList<File> listFile = new ArrayList<File>();
		HashMap<String, String> branchDefMap = new HashMap<String, String>();
		HashMap<String, Long> sumMap = new HashMap<String, Long>();

		if(args.length != 1){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		//支店定義ファイル読み込みメソッド呼び出し
		if(! readDefine(args[0], branchDefMap, sumMap)){
			return;
		}

		//集計メソッド呼び出し
		if(! readData(args[0], listFile, branchDefMap, sumMap)){
			return;
		}

		//出力ファイルメソッド呼び出し
		if(!readOutput(args[0], branchDefMap, sumMap)){
			return;
		}
	}
	private static boolean readDefine(String path, HashMap<String, String> DefMap, HashMap<String, Long> sumDefMap){

		BufferedReader br = null;
		try {

			File file = new File(path, "branch.lst");

			if(!file.exists()){
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;

			//支店定義ファイルを1行ずつ読み込み
			while((line = br.readLine()) != null){
				//支店コードと名前を分ける
				String[] branchDef = line.split(",");

				if(!branchDef[0].matches("^[0-9]{3}")){
					//マップに支店コードと名前を入れる
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				if(branchDef.length != 2){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				DefMap.put(branchDef[0], branchDef[1]);
				sumDefMap.put(branchDef[0], 0L);
			}

		} catch(IOException e) {
			System.out.println("支店名義ファイルが存在しません");
			return false;
		} finally {

			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}

		return true;
	}
	private static boolean readData(String path, ArrayList<File> listFile, HashMap<String, String> DefMap, HashMap<String, Long> sumDefMap) throws IOException{

		BufferedReader cr = null;
		File dir = new File(path);
		File[]fileNames = dir.listFiles();
		String branchCodeName;
		String branchCodeChe;

		//ファイルの数だけループ
		for(int i = 0; i < fileNames.length; i++){

			//ファイル名が8桁且つ拡張子が.rcdですべてファイルであるか
			if(fileNames[i].isFile()){
				if(fileNames[i].getName().matches("^[0-9]{8}.rcd$") ){
					//listFileにファイルを格納
					listFile.add(fileNames[i]);
				}
			}
		}

		for(int i = 0; i < (listFile.size())-1; i++){
			//連番チェックのための名前取得
			branchCodeName = listFile.get(i).getName();
			branchCodeChe = listFile.get(i+1).getName();
			String branchCode = branchCodeName.substring(0,8);
			String branchCodeNames = branchCodeChe.substring(0,8);
			int branchVal = Integer.parseInt(branchCode);
			int branchValChe = Integer.parseInt(branchCodeNames);

			//連番チェック
			if((branchVal ) != (branchValChe -1)){
				System.out.println("売上ファイル名が連番になっていません");
				return false;
			}

		}

		try{

			//ファイルの数だけループ
			for(int i = 0; i < listFile.size(); i++){

				ArrayList<String> extractFile = new ArrayList<String>();
				cr = new BufferedReader(new FileReader(listFile.get(i)));
				String lines;
				//1行ずつ売り上げファイルを読み込み
				while((lines = cr.readLine()) != null){
					extractFile.add(lines);
					}
				//2行以外ならエラー
				if(extractFile.size() != 2){
					System.out.println(listFile.get(i).getName() + "のフォーマットが不正です");
					return false;
				}
				Long sum = 0L;

				//マップ内にコードが存在しているか
				if(!DefMap.containsKey(extractFile.get(0))){
					System.out.println(listFile.get(i).getName() + "の支店コードが不正です");
					return false;
				}
					//同じコードが存在したら加算
				sum = sumDefMap.get(extractFile.get(0)) + Long.parseLong(extractFile.get(1)) ;
					//マップにコードと売り上げを格納

				if(sum.toString().length() > 10){
					System.out.println("合計金額が10桁を超えました");
					return false;

					}
				if(sum < 0){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
				sumDefMap.put(extractFile.get(0), sum);
			}


		}catch (IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}catch (Exception err){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally{
			if(cr != null) {
				try {
					cr.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;



	}
	private static boolean readOutput(String path,  HashMap<String, String> DefMap, HashMap<String, Long> sumDefMap) throws IOException{
		BufferedWriter bw =null;

		try{
			File pwFile = new File(path,"branch.out");
			FileWriter fw = new FileWriter(pwFile);
			//ファイルを書き込むための変数
			bw = new BufferedWriter(fw);

			//該当する支店コードの数だけループ
			for(HashMap.Entry<String, String> branchPut :DefMap.entrySet() ){
				bw.write(branchPut.getKey() + "," + branchPut.getValue() + "," + sumDefMap.get(branchPut.getKey()));		//ファイル書き込み
				bw.newLine();
			}

		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;

		}finally{
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}


		return true;
	}
}
